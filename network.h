#ifndef NETWORK_DEPENDENCY
#define NETWORK_DEPENDENCY

#include "interfaces.h"
typedef void (*search_type)(const char *, const char *, char *);
//structure passed as an argument to send_message function
struct send_message_args {
	char * message;
	long timer;
	int * check_stop;
};
//function send message to remoute host evry timer milliseconds
thread_f_out send_message(void * arg); // - 2 hour
//start server with the port
//reads file and search path information and sends a message to the client
//wait search information
void server(short port); // 4 - hour
//start client
//wait search information from server
//wait end of send_message thread
//start send_message thread
//start serch_func
//sends result of searching to the server
//serch_func - search function
//search result 3rd argument
void client(char * ip, short port, search_type search_func); // 4 - hour
#endif