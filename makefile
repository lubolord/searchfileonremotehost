all: clean build
main.o: main.c
	gcc -c -o main.o main.c
interfaces.o: interfaces.c
	gcc -c -o interfaces.o interfaces.c
network.o: network.c
	gcc -c -o network.o network.c
clean:
	rm main.o network.o interfaces.o

ifeq ($(OS),Windows_NT)
build: main.o network.o interfaces.o 
	gcc -o SearchFIleOnRemoteHost main.o network.o interfaces.o -lws2_32
else
build: main.o network.o interfaces.o
	gcc -o SearchFIleOnRemoteHost main.o network.o interfaces.o -lpthread
endif