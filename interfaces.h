#ifndef PL_DEPENDENCY
#define PL_DEPENDENCY

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32

#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>

//dirrectory

typedef WIN32_FIND_DATAA FIND_DATA;
#define SLESH_SYMBOL '\\'

//threads

typedef HANDLE thread;
#define thread_f_out DWORD WINAPI

#endif

#ifdef __unix__
#include <unistd.h>
#include <limits.h>
#include <sys/types.h>

//dirrectory
#include <dirent.h>

#define MAX_PATH PATH_MAX

#define HANDLE DIR *
#define FIND_DATA struct dirent *
#define SLESH_SYMBOL '/'

//network
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

typedef int SOCKET;

//thread
#include <pthread.h>

typedef pthread_t thread;
#define thread_f_out void *

#endif

//dirrectory - 0.5 hour

//Searches the first file in the directory and opens the stream
//In windows wrapper over FINDFIRSTFILEA
//In linux wrapper over opendir and readdir
int find_first_file(HANDLE *h, FIND_DATA *fd, char * path);
//Searches the next file in the directory stream
//In windows wrapper over FINDNEXTFILEA
//In linux wrapper over readdir
int find_next_file(HANDLE h, FIND_DATA *fd);
//returns the file name from the platform dependent structure
char * get_name(FIND_DATA *fd);
//In windows wrapper over FINDCLOSE
//In linux wrapper over closedir
void find_close(HANDLE h);
//returns the current directory
//In windows wrapper over GetCurrentDirectoryA
//In linux wrapper over getcwd
int get_current_dir(long len, char * path);
//rewind directory stream
//In windows wrapper over FlushFileBuffers
//In linux wrapper over rewinddir
void rewind_find_file(HANDLE h);


//network - 0.5 hour

//Create socket
//In windows start WSA and wrapper over socket
//In linux wrapper over socket
SOCKET Socket(int af, int type, int protocol);
//Check socket error(in windows INVALID_SOCKET)
int is_INVALID_SOCKET(SOCKET sock);
//Check socket error(in windows SOCKET_ERROR)
int is_SOCKET_ERROR(int connect_out);
//close socket
//In windows stops WSA and wrapper over closesocket
//In linux wrapper over close
int close_socket(SOCKET sock);

//thread - 0.5 hour

//Create thread
//In windows wrapper over CreateThread
//In linux wrapper over pthread_create
HANDLE thread_create(thread *th, thread_f_out (*func)(void *), void * arg);
//Exit thread
//In windows wrapper over ExitThread
//In linux wrapper over pthread_exit
void thread_exit(long dwExitCode);
//function is waiting for thread to finish
//In windows wrapper over WaitForSingleObject
//In linux wrapper over pthread_join
void thread_join(thread th);
//Stop thread by msec milliseconds
//In windows wrapper over Sleep
//In linux wrapper over usleep
void thread_sleep(long msec);

#endif